# Jogo Gourmet
Projeto criado para o desafio da Objective Solutions

### Pré-requisitos

* Java 14
* Maven 3

### Modelo

![modelo](https://i.imgur.com/tiIofTP.png)

### Binário

[Download Aqui](JogoGourmet.jar)
