package br.com.objective;

import br.com.objective.UI.InterfaceGrafica;
import br.com.objective.UI.InterfaceDeUsuario;

public class Principal {
    public static void main(String[] args) {
        InterfaceDeUsuario interfaceDeUsuario = new InterfaceGrafica();

        try {
            Perguntas perguntas = new Perguntas("massa", "Bolo de Chocolate", "lasanha");
            Jogo game = new Jogo(interfaceDeUsuario, perguntas);
            game.iniciar();
        } catch (Exception e) {
            interfaceDeUsuario.exibirTexto("Oops, ocorreu um erro na execução do jogo.");
        }
    }
}