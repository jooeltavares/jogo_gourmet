package br.com.objective;

import java.util.Arrays;
import java.util.Optional;

public enum Resposta {
    SAIR(-1), SIM(0), NAO(1);

    private final int valor;

    Resposta(int valor) {
        this.valor = valor;
    }

    public static Optional<Resposta> valueOf(int valor) {
        return Arrays.stream(values())
                .filter(resposta -> resposta.valor == valor)
                .findFirst();
    }
}
