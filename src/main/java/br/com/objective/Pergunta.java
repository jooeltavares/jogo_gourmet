package br.com.objective;

public class Pergunta {
    private String valor;
    private Pergunta direita;
    private Pergunta esquerda;
    private Pergunta pai;

    public Pergunta(String valor) {
        this.valor = valor;
        this.esquerda = this.direita = this.pai = null;
    }

    public Pergunta(String valor, Pergunta direita, Pergunta esquerda, Pergunta pai) {
        this.valor = valor;
        this.direita = direita;
        this.esquerda = esquerda;
        this.pai = pai;
    }

    public String getValor() {
        return valor;
    }

    public Pergunta getDireita() {
        return direita;
    }

    public void setDireita(Pergunta direita) {
        this.direita = direita;
    }

    public Pergunta getEsquerda() {
        return esquerda;
    }

    public void setEsquerda(Pergunta esquerda) {
        this.esquerda = esquerda;
    }

    public Pergunta getPai() {
        return pai;
    }

    public void setPai(Pergunta pai) {
        this.pai = pai;
    }

    public Pergunta getResposta(Resposta resposta) {
        return resposta == Resposta.SIM ? this.getEsquerda() : this.getDireita();
    }
}
