package br.com.objective;

import br.com.objective.UI.InterfaceDeUsuario;

import java.util.Optional;

public class Jogo {
    private InterfaceDeUsuario interfaceDeUsuario;
    private Perguntas perguntas;

    public Jogo(InterfaceDeUsuario interfaceDeUsuario, Perguntas perguntas) {
        this.interfaceDeUsuario = interfaceDeUsuario;
        this.perguntas = perguntas;
    }

    public void iniciar() {
        while (interfaceDeUsuario.exibirTexto("Pense em um prato que gosta.") != -1) {
            this.executar();
        }
    }

    private void executar() {
        Pergunta perguntaAtual = this.perguntas.getPerguntaPrincipal();

        while (perguntaAtual != null) {
            Resposta resposta = recuperarReposta(perguntaAtual);

            if (resposta == null) {
                break;
            }

            perguntaAtual = processarResposta(perguntaAtual, resposta);
        }
    }

    private Resposta recuperarReposta(Pergunta perguntaAtual) {
        int retorno = interfaceDeUsuario.exibirPergunta("O prato que você pensou é " + perguntaAtual.getValor() + "?");
        Optional<Resposta> optionalResposta = Resposta.valueOf(retorno);

        if (!optionalResposta.isPresent()) {
            return null;
        }

        Resposta resposta = optionalResposta.get();

        if (resposta == Resposta.SAIR) {
            return null;
        }

        return resposta;
    }

    private Pergunta processarResposta(Pergunta perguntaAtual, Resposta resposta) {
        Pergunta pergunta = perguntaAtual.getResposta(resposta);

        if (resposta == Resposta.SIM && null == pergunta) {
            interfaceDeUsuario.exibirTexto("Acertei de novo!");
        } else if (resposta == Resposta.NAO && null == pergunta) {
            String prato = interfaceDeUsuario.lerPergunta("Qual prato você pensou?");
            String caracteristica = interfaceDeUsuario.lerPergunta(prato + " é ____ mas " + perguntaAtual.getValor() + " não.");
            this.perguntas.criarNovaPergunta(caracteristica, prato, perguntaAtual);
        }

        return pergunta;
    }
}