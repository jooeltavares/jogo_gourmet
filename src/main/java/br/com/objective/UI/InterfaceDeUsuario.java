package br.com.objective.UI;

public interface InterfaceDeUsuario {
    int exibirTexto(String mensagem);
    int exibirPergunta(String mensagem);
    String lerPergunta(String mensagem);
}
