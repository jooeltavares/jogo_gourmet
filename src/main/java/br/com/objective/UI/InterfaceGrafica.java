package br.com.objective.UI;

import javax.swing.*;

public class InterfaceGrafica implements InterfaceDeUsuario {
    @Override
    public int exibirTexto(String mensagem) {
        return JOptionPane.showConfirmDialog(null, mensagem, "Jogo Gourmet", JOptionPane.DEFAULT_OPTION);
    }

    @Override
    public int exibirPergunta(String mensgem) {
        return JOptionPane.showConfirmDialog(
                null,
                mensgem,
                "Confirm",
                JOptionPane.YES_NO_OPTION);
    }

    @Override
    public String lerPergunta(String mesangem) {
        return JOptionPane.showInputDialog(mesangem);
    }
}
