package br.com.objective.UI;

import br.com.objective.Resposta;

import java.util.Scanner;

public class InterfaceTexto implements InterfaceDeUsuario {
    private Scanner scanner;

    public InterfaceTexto() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public int exibirTexto(String mensagem) {
        return processarReposta(lerPergunta(mensagem));
    }

    private int processarReposta(String opcao) {
        opcao = opcao.toUpperCase();

        int retorno = Resposta.SAIR.ordinal();

        if (opcao.equals("NAO")) {
            retorno = Resposta.NAO.ordinal();
        } else if (opcao.equals("SIM")) {
            retorno = Resposta.SIM.ordinal();
        }

        return --retorno;
    }

    @Override
    public int exibirPergunta(String mensagem) {
        return exibirTexto(mensagem);
    }

    @Override
    public String lerPergunta(String mensagem) {
        System.out.println(mensagem);

        return scanner.nextLine();
    }
}
