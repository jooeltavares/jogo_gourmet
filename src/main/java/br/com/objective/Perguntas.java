package br.com.objective;

public class Perguntas {
    private Pergunta principal;

    public Perguntas(String pergunta, String direita, String esquerda) {
        this.principal = new Pergunta(pergunta, new Pergunta(direita), new Pergunta(esquerda), null);
        this.principal.getEsquerda().setPai(this.principal);
        this.principal.getDireita().setPai(this.principal);
    }

    public Pergunta getPerguntaPrincipal() {
        return this.principal;
    }

    public Pergunta criarNovaPergunta(String pergunta, String nomePrato, Pergunta perguntaAtual) {
        Pergunta novaPergunta = new Pergunta(pergunta, perguntaAtual,  new Pergunta(nomePrato), perguntaAtual.getPai());
        Pergunta pai = perguntaAtual.getPai();

        if (pai.getDireita() == perguntaAtual) {
            pai.setDireita(novaPergunta);
        } else {
            pai.setEsquerda(novaPergunta);
        }

        novaPergunta.getDireita().setPai(novaPergunta);
        novaPergunta.getEsquerda().setPai(novaPergunta);

        return novaPergunta;
    }
}
