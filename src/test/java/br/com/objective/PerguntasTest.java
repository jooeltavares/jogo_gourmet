package br.com.objective;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PerguntasTest {
    private final Perguntas perguntas = new Perguntas("massa", "Bolo de Chocolate", "lasanha");

    @Test
    public void validarNoDaDireita() {
        Pergunta perguntaPrincipal = perguntas.getPerguntaPrincipal();

        assertEquals("Bolo de Chocolate", perguntaPrincipal.getDireita().getValor());
    }

    @Test
    public void validarNoDaEsquerda() {
        Pergunta perguntaPrincipal = perguntas.getPerguntaPrincipal();

        assertEquals("lasanha", perguntaPrincipal.getEsquerda().getValor());
    }

    @Test
    public void validarNoRaiz() {
        assertEquals("massa", perguntas.getPerguntaPrincipal().getValor());
    }

    @Test
    public void validarCriacaoDeNovaPergunta() {
        Pergunta perguntAtual = perguntas.getPerguntaPrincipal().getDireita();
        Pergunta paiDaPerguntaAtual = perguntAtual.getPai();
        Pergunta novaPergunta = perguntas.criarNovaPergunta("gelado", "Sorvete", perguntAtual);

        assertEquals(paiDaPerguntaAtual, novaPergunta.getPai());
        assertEquals(perguntAtual, novaPergunta.getDireita());
    }
}
